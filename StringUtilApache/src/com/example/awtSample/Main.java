package com.example.awtSample;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

	        String s1 = new String("  Java Language ");
            String s2 =new String();
            String s3 = new String("james gosling ");
            
            StringUtils.isEmpty(s1);


            System.out.println("isNotEmpty"+StringUtils.isNotEmpty(s1));


            System.out.println("String is Blank="+StringUtils.isBlank(s1));


            System.out.println("String is Not empty="+StringUtils.isNotBlank(s1));   // true

            //trim()
             String sTrim = StringUtils.trim(s1);
             System.out.println("sTrim()"+sTrim);
            //System.out.println("trim() ="+StringUtils.trim(s1));

            //strip()
            String sStrip= StringUtils.trim(s1);
            System.out.println("strip()"+sStrip);
            //System.out.println("strip() ="+StringUtils.strip(s1));


            //equals();
            System.out.println("String Equals(s1,s2)="+StringUtils.equals(s1,s3));
            String s4 = new String("  Java Language ");
            System.out.println("String Equals(s1,s4)="+StringUtils.equals(s1,s4));

            //compare();
            System.out.println("String  Compare(s1,s2)="+StringUtils.compare(s1,s3));
            System.out.println("String  compare(s1,s4)="+StringUtils.compare(s1,s4));

            String s5= new String("java");
            //startwith
            System.out.println("StartsWith = "+StringUtils.startsWith(s5,"j"));

            //endsWith
            System.out.println("endsWith = "+StringUtils.endsWith(s5,"a"));

            //IndexOf()
            System.out.println("IndexOf="+StringUtils.indexOf(s5,"a"));

            // isNumeric();
            System.out.println("String is Numeric= "+StringUtils.isNumeric(s1));

            //join();
            List<String> names= Arrays.asList("java","javascipt","c");
            System.out.println("String Joined with comma ="+StringUtils.join(names," "));
            System.out.println("String Joined with comma ="+StringUtils.join(names,","));

            //without SstringUtils()
             System.out.println("without StringUtils join = "+String.join(",",names));

            //abbreviate();
            String abbreviated = StringUtils.abbreviate(s1,12);
            System.out.println("abbreviate = "+abbreviated);

    }
}
